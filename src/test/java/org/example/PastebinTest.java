package org.example;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PastebinTest {
    private WebDriver driver;
    private PastebinHomePage PastebinHomePage;

    @BeforeClass
    public void setUp() {
        driver = new ChromeDriver();
        driver.get("https://pastebin.com");
        PastebinHomePage = new PastebinHomePage(driver);
    }

    @Test
    public void testCreateNewPaste() {
        PastebinHomePage.createNewPaste("Hello from WebDriver", "helloweb");

    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
