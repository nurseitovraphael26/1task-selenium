package org.example;

import org.openqa.selenium.By;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PastebinHomePage {
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(id = "postform-text")
    private WebElement pasteCode;

    @FindBy(xpath = "//label[contains(text(),'Paste Expiration')]/following-sibling::div//span[@role='combobox']")
    private WebElement expirationDropdown;

    @FindBy(xpath = "//li[contains(text(),'10 Minutes')]")
    private WebElement tenMinutesOption;

    @FindBy(id = "postform-name")
    private WebElement pasteName;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement submitButton;

    public PastebinHomePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(2));
        PageFactory.initElements(driver, this);
    }

    public void createNewPaste(String code, String title) {
        pasteCode.sendKeys(code);
        expirationDropdown.click();
        wait.until(ExpectedConditions.visibilityOf(tenMinutesOption)).click();
        pasteName.sendKeys(title);
        submitButton.click();
    }
}

